﻿using System;
using System.Security.Cryptography;
using UnityEngine;
using Object = System.Object;

namespace _Scripts.Controls.BuildingSystem
{
    public class BuildingPlacer : MonoBehaviour
    {
        public bool InBuildingMode = false;

        public GameObject BuildingOne;
        public GameObject BuildingTwo;
        private GameObject currentSelectedBuilding;

        public LayerMask Mask;
        private int laserPosX, laserPosY, laserPosZ = 0;
        private Vector3 mousePos;

        // Update is called once per frame
        void Update()
        {
            ActivateConstructionMode();
            if (!InBuildingMode)
                return;
            ChooseBuilding();
            if (currentSelectedBuilding == null)
                return;


            mousePos = Input.mousePosition;
            Ray ray = Camera.main.ScreenPointToRay(mousePos);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, Mathf.Infinity, Mask))
            {
                int posX = (int) Mathf.Round(hit.point.x);
                int posZ = (int) Mathf.Round(hit.point.z);

                if (posX != laserPosX || posZ != laserPosZ)
                {
                    //place the obj on ground
                    laserPosX = posX;
                    laserPosZ = posZ;
                    Vector3 sizeVec = currentSelectedBuilding.GetComponent<MeshRenderer>().bounds.extents;
                    currentSelectedBuilding.transform.position = new Vector3(posX, sizeVec.y, posZ);
                }
            }

            if (Input.GetMouseButtonDown(0))
            {
                PlaceBuilding();
            }
        }

        private void PlaceBuilding()
        {
            //if there is nothing in the way, place it
            MaterialSetter currentMaterial = currentSelectedBuilding.GetComponent<MaterialSetter>();
            if (currentMaterial.ObjectCount <= 1)
            {
                //set the material to default and delete the material setter
                GameObject tempGameObject = Instantiate(currentSelectedBuilding,
                    currentSelectedBuilding.transform.position, Quaternion.identity);
                MaterialSetter tempMaterialSetter = tempGameObject.GetComponent<MaterialSetter>();
                tempMaterialSetter.SetMaterial(tempMaterialSetter.MatDefault);
                Destroy(tempMaterialSetter);
            }
        }

        private void ChooseBuilding()
        {
            if (Input.GetKeyDown("1"))
            {
                Destroy(currentSelectedBuilding);
                currentSelectedBuilding = Instantiate(BuildingOne, mousePos, Quaternion.identity);
            }

            if (Input.GetKeyDown("2"))
            {
                Destroy(currentSelectedBuilding);
                currentSelectedBuilding = Instantiate(BuildingTwo, mousePos, Quaternion.identity);
            }
        }

        private void ActivateConstructionMode()
        {
            if (Input.GetKeyDown(KeyCode.Tab))
            {
                if (InBuildingMode)
                    Destroy(currentSelectedBuilding);

                InBuildingMode = !InBuildingMode;
            }
        }
    }
}