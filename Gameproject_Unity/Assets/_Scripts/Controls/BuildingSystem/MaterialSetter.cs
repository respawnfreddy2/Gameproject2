﻿using System;
using System.Security.Cryptography;
using UnityEngine;

namespace _Scripts.Controls.BuildingSystem
{
    public class MaterialSetter : MonoBehaviour
    {
        public Material MatCanPlace, MatPlacingFailed, MatDefault;
        public bool CanPlace;
        public Renderer Rend;
        public int ObjectCount;

        // Start is called before the first frame update
        void Start()
        {
            Rend = GetComponent<Renderer>();
            Rend.enabled = true;
        }

        public void SetMaterial(Material material)
        {
            Rend.sharedMaterial = material;
            foreach (Transform child in transform)
            {
                child.GetComponent<Renderer>().material = material;
            }
        }


        // Update is called once per frame
        void FixedUpdate()
        {
            if (ObjectCount > 1)
            {
                SetMaterial(MatPlacingFailed);
                CanPlace = false;
            }
            else if (ObjectCount <= 1)
            {
                SetMaterial(MatCanPlace);
                CanPlace = true;
            }
        }

        private void OnCollisionEnter(Collision collision)
        {
            ObjectCount++;
        }

        private void OnCollisionExit(Collision collision)
        {
            ObjectCount--;
        }
    }
}