﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using _Scripts.Controls.BuildingSystem;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class ConstructionUI : MonoBehaviour
{
    // Start is called before the first frame update
    public Text TextField;
    public GameObject BuildingSystem;
    private BuildingPlacer buildingPlacer;
    
    void Start()
    {
        buildingPlacer = BuildingSystem.GetComponent<BuildingPlacer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!buildingPlacer.InBuildingMode)
        {
            TextField.text = "ConstructionMode is off (Tab)";
        }
        else
        {
            TextField.text = "ConstructionMode is on\n 1 : Cube";
            
        }
    }
}
