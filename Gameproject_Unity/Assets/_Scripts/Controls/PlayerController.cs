﻿using System.Collections;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float moveSpeed;
    public bool useController;
    public Animator animator;

    private Rigidbody characterRigidbody;
    private Vector3 moveInput;
    private Vector3 moveVelocity;
    private Camera mainCamera;
    private bool isAlive;


    void Start() {
        // setting up variables
        characterRigidbody = GetComponent<Rigidbody>();
        mainCamera = FindObjectOfType<Camera>();
        isAlive = true;
    }

    void Update () {
        // setting up detection loops
        if (isAlive) {
            if (!useController) {
                MouseAndKeyboardControls();
            }
            else {
                GamepadControls();
            }
        }
    }

    private void MouseAndKeyboardControls() {
        // WASD detection
        if (Input.GetAxisRaw("Horizontal") != 0 || Input.GetAxisRaw("Vertical") != 0) {
            Running();
        }
        else {
            animator.SetBool("running", false);
        }

        // left mouse click detection
        if (Input.GetMouseButtonDown(0)) {
            Shooting();
        }

        // right mouse click detection
        if (Input.GetMouseButton(1)) {
            Aiming();
        }
        else {
            animator.SetBool("aiming", false);
            moveSpeed = 6;
        }
    }

    private void GamepadControls() {
        // left stick input detection
        if (Input.GetAxisRaw("Horizontal") != 0 || Input.GetAxisRaw("Vertical") != 0) {
            Running();
        }
        else {
            animator.SetBool("running", false);
        }

        // right stick detection
        if (Input.GetAxis("RHorizontal") != 0 || Input.GetAxis("RVertical") != 0) {
            Aiming();
        }
        else {
            animator.SetBool("aiming", false);
            moveSpeed = 6;
        }
    }

    private void Running() {
        moveInput = new Vector3(Input.GetAxisRaw("Horizontal"), 0f, Input.GetAxisRaw("Vertical"));
        moveVelocity = moveInput * moveSpeed;
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");

        Vector3 moveAngle = Vector3.right * x + Vector3.forward * y;
        transform.rotation = Quaternion.LookRotation(moveAngle, Vector3.up);

        characterRigidbody.velocity = moveVelocity;
        animator.SetBool("running", true);
    }

    private void Aiming() {
        moveSpeed = 3;

        if (!useController) {
            // aiming with Mouse
            Ray cameraRay = mainCamera.ScreenPointToRay(Input.mousePosition);
            Plane groundPlane = new Plane(Vector3.up, Vector3.zero);
            float rayLength;

            if (groundPlane.Raycast(cameraRay, out rayLength)) {
                Vector3 pointToLook = cameraRay.GetPoint(rayLength);
                Debug.DrawLine(cameraRay.origin, pointToLook, Color.blue);

                transform.LookAt(new Vector3(pointToLook.x, transform.position.y, pointToLook.z));
            }
        }
        else {
            // aiming with right stick
            float x = Input.GetAxis("RHorizontal");
            float y = Input.GetAxis("RVertical");

            if (x != 0 || y != 0) {
                Vector3 aimAngle = Vector3.right * x + Vector3.forward * -y;
                transform.rotation = Quaternion.LookRotation(aimAngle, Vector3.up);
            }
        }

        animator.SetBool("aiming", true);
    }

    private void Shooting() {

    }
}
